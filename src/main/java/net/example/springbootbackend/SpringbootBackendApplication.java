package net.example.springbootbackend;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import net.example.springbootbackend.model.Employee;
import net.example.springbootbackend.repository.EmployeeRepository;

@SpringBootApplication
public class SpringbootBackendApplication implements CommandLineRunner{

	public static void main(String[] args) {
		SpringApplication.run(SpringbootBackendApplication.class, args);
	}
	
	@Autowired
	private EmployeeRepository employeeRepository;
	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
//		Employee employee = new Employee();
//		employee.setEmailId("espinozaramirezjuan119@gmail.com");
//		employee.setFirstName("Juan Angel");
//		employeeRepository.save(employee);
//		Employee employee1 = new Employee();
//		employee1.setEmailId("marcos_juarez@gmail.com");
//		employee1.setFirstName("Marcos Juarez");
//		employeeRepository.save(employee1);
	}

}
